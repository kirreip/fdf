/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/26 13:26:42 by pmartin           #+#    #+#             */
/*   Updated: 2015/05/12 23:11:22 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void ft_line4(t_var var, t_map *elem, int x)
{
	if (x > 0)
		ft_line4(var, elem, x - 1);
	while (elem->x < x)
		elem = elem->nxt;
	var.x2 = (elem->x + elem->z * var.cst[0]) * var.u + var.i;
	var.y2 = (elem->y + elem->z * var.cst[1]) * var.u + var.j;
	while (elem)
	{
		var.x1 = var.x2;
		var.y1 = var.y2;
		var.temp = elem;
		elem = elem->nxt;
		while (elem && elem->x != x)
			elem = elem->nxt;
		if (elem)
		{
			var.x2 = (elem->x + elem->z * var.cst[0]) * var.u + var.i;
			var.y2 = (elem->y + elem->z * var.cst[1]) * var.u + var.j;
			if (var.temp->y + 1 == elem->y)
				ft_makeline(var);
		}
	}
}

void ft_line3(t_var var, t_map *elem)
{
	t_map *temp;

	var.xmax = 0;
	temp = elem;
	while (elem->nxt)
	{
		if (elem->nxt->x > var.xmax)
			var.xmax = elem->nxt->x;
		var.x1 = (elem->x + elem->z * var.cst[0]) * var.u + var.i;
		var.x2 = (elem->nxt->x + var.cst[0]
				* elem->nxt->z) * var.u + var.i;
		var.y1 = (elem->y + elem->z * var.cst[1]) * var.u + var.j;
		var.y2 = (elem->nxt->y + elem->nxt->z * var.cst[1])
			* var.u + var.j;
		if (elem->y == elem->nxt->y && elem->x == (elem->nxt->x - 1))
			ft_makeline(var);
		elem = elem->nxt;
	}
	ft_line4(var, temp, var.xmax);
}
