/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strfdf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/06 17:29:30 by pmartin           #+#    #+#             */
/*   Updated: 2015/05/12 03:24:41 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static int	ft_celem(char *s)
{
	int i;
	int k;

	i = 0;
	k = 0;
	while (s[i])
		if (ft_isdigit(s[i]) || s[i++] == '-')
		{
			while (ft_isdigit(s[i]) || s[i] == '-')
				i++;
			k++;
		}
	return (k);
}

static int	ft_cc(char *s)
{
	int i;

	i = 0;
	while (s[i] && (ft_isdigit(s[i]) || s[i] == '-'))
		i++;
	return (i);
}

char		**ft_strfdf(char *s)
{
	int		i;
	int		j;
	int		u;
	int		nbelem;
	char	**str;

	i = 0;
	u = 0;
	nbelem = ft_celem(s);
	if (!(str = (char **)malloc(sizeof(*str) * (nbelem + 1))))
		return (0);
	while (u < ft_celem(s) && s[i])
	{
		j = 0;
		while (s[i] && ft_isdigit(s[i]) == 0 && s[i] != '-')
			i++;
		if (!(str[u] = (char *)malloc(sizeof(char) * ft_cc(&s[i]))))
			return (0);
		while (ft_isdigit(s[i]) || s[i] == '-')
			str[u][j++] = s[i++];
		str[u++][j] = 0;
		i++;
	}
	str[u] = 0;
	return (str);
}
