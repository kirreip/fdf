/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/05 17:03:37 by pmartin           #+#    #+#             */
/*   Updated: 2015/05/12 23:46:20 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		expose_hook(t_var *var)
{
	mlx_destroy_image(var->mlx_ptr, var->img_ptr);
	var->img_ptr = mlx_new_image(var->mlx_ptr, 2000, 1200);
	var->data = mlx_get_data_addr(var->img_ptr, &var->bpp,
	&var->sizeline, &var->endian);
	mlx_clear_window(var->mlx_ptr, var->win_ptr);
	if (var->elem->nxt)
	{
		if (var->project == 1)
			ft_line(*(var), var->elem);
		else if (var->project == 0)
			ft_line3(*(var), var->elem);
		mlx_put_image_to_window(var->mlx_ptr,
								var->win_ptr, var->img_ptr, 0, 0);
	}
	else
		mlx_pixel_put(var->mlx_ptr, var->win_ptr, var->i, var->j, 0xFFFFFF);
	return (0);
}

void	ft_pixel_put(t_var var)
{
	int o;

	o = mlx_get_color_value(var.mlx_ptr, 0x66FF66);
	ft_memcpy(var.data + (var.xtemp * (var.bpp / 8))
			+ (var.ytemp * var.sizeline), &o, 3);
}

int		key_hook(int k, t_var *var)
{
	(k == 53) ? exit(1) : k;
	(k == 126) ? (var->j -= 50) : k;
	(k == 123) ? (var->i -= 50) : k;
	(k == 125) ? (var->j += 50) : k;
	(k == 124) ? (var->i += 50) : k;
	(k == 83) ? (var->project = 1) : k;
	(k == 84) ? (var->project = 0) : k;
	if (k == 78)
		if (var->u * 0.67 > 2)
			var->u = var->u * 0.67;
	if (k == 69)
		var->u = var->u * 1.5;
	expose_hook(var);
	return (0);
}
