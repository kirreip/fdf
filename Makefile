#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/05/11 18:23:17 by pmartin           #+#    #+#              #
#    Updated: 2015/05/12 21:12:11 by pmartin          ###   ########.fr        #
#                                                                              #
#******************************************************************************#


NAME = fdf
CC = cc
LMLX = -lmlx -framework OpenGL -framework AppKit -L/usr/X11/lib
FLAGS = -Wall -Wextra -Werror
IDIR = includes/
SDIR = src/
LIBFT = libft/
SRC = main.c \
	print.c \
	key.c \
	get_next_line.c \
	ft_strfdf.c \
	list.c \
	print2.c
OBJ = $(SRC:.c=.o)

all: $(NAME)

$(NAME): $(OBJ)
	@make -C $(LIBFT) re
	@$(CC) -o $@ $^ $(FLAGS) $(LIBFT)libft.a $(LMLX) -I $(IDIR)

%.o: %.c
	@$(CC) $(FLAGS) -o $@ -c $< -I $(IDIR)

clean:
	@rm -f $(OBJ)

fclean: clean
	@rm -f $(NAME)
	@make -C $(LIBFT) fclean

re: fclean all

.PHONY: all $(NAME) clean fclean re