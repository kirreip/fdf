/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/11 18:23:35 by pmartin           #+#    #+#             */
/*   Updated: 2015/05/12 21:09:54 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	freetab(char ***tab)
{
	int i;

	i = 0;
	while ((*tab)[i])
		free((*tab)[i++]);
	free(*tab);
}

t_map	*ft_setmap(char *str, int nbline, t_map **elem)
{
	int			i;
	char		**tab;
	static int	k;

	i = 0;
	if (k == 0)
	{
		k = 1;
		if (!(elem[0] = (t_map *)malloc(sizeof(*elem[0]))))
			return (0);
		elem[1] = elem[0];
	}
	if (!(tab = ft_strfdf(str)))
		return (0);
	while (tab[i++])
		elem[0] = newelem(i - 1, nbline, ft_atoi(tab[i - 1]), elem[0]);
	freetab(&tab);
	return (elem[0]);
}

int		main(int ac, char **av)
{
	int		fd;
	char	*str;
	int		i;
	t_map	*elem[2];
	t_var	var;

	i = 0;
	str = 0;
	if (ac == 2)
	{
		fd = open(av[1], O_RDONLY);
		while (get_next_line(fd, &str) == 1)
			elem[0] = ft_setmap(str, i++, elem);
		ft_setmap(str, i++, elem);
		var.elem = elem[1];
		var.i = 1000;
		var.j = 200;
		var.u = 20;
		var.project = 1;
		ft_printmap(elem[1], var);
	}
	return (0);
}
