/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/14 09:40:05 by pmartin           #+#    #+#             */
/*   Updated: 2015/05/11 16:00:06 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"
#include "libft/includes/libft.h"

static void		ft_init(t_cheat *truc, int o)
{
	if (o == 1)
	{
		(*truc).temp = ft_strnew(1);
		(*truc).buff = ft_strnew(BUFF_SIZE + 1);
		(*truc).i = 0;
		(*truc).j = 0;
	}
	else if (o == 2)
	{
		(*truc).j = 1;
		(*truc).i = 0;
	}
}

static int		ft_up(t_cheat *truc, char **line, char **s)
{
	int temp;
	int i;

	i = 0;
	temp = (*line) ? ft_strlen(*line) : 1;
	if (!((*truc).temp = ft_strnew(temp + 1)))
		return (-1);
	ft_strcpy((*truc).temp, *line);
	if (*s != 0)
		free(&*line);
	if (!(*line = ft_strnew(temp + BUFF_SIZE + 1)))
		return (-1);
	ft_strcpy(*line, (*truc).temp);
	free((*truc).temp);
	while ((*truc).buff[i] != '\n' && (*truc).buff[i])
		i++;
	ft_strncat(*line, (*truc).buff, i);
	if ((*truc).buff[i] == '\0')
	{
		free((*truc).buff);
		(*truc).buff = ft_strnew(BUFF_SIZE + 3);
	}
	*s = (!((*truc).buff[i])) ? 0 : (ft_strdup(&((*truc).buff[i + 1])));
	return (1);
}

static int		ft_check(char *s)
{
	while (*s)
		if (*s++ == '\n')
			return (0);
	return (1);
}

static int		ft_all(int fd, t_cheat *truc, char **line, char **s)
{
	char *temp;

	ft_init(truc, 2);
	temp = *s;
	if (temp)
	{
		if (!(*line = ft_strnew(BUFF_SIZE + 1)))
			return (-1);
		while (temp[(*truc).i] && temp[(*truc).i] != '\n')
			(*truc).i++;
		ft_strncpy(*line, temp, (*truc).i);
		if (temp[(*truc).i] == '\0')
			*s = 0;
		else
			*s = ft_strdup(&temp[(*truc).i + 1]);
	}
	if (temp == 0 || temp[(*truc).i] != '\n')
		while (((*truc).j = read(fd, (*truc).buff, BUFF_SIZE)))
		{
			if (ft_up(truc, line, s) == -1)
				return (-1);
			if (ft_check((*truc).buff) == 0)
				return (1);
		}
	return (((*truc).j) ? 1 : 0);
}

int				get_next_line(int const fd, char **line)
{
	t_cheat		truc;
	char		test;
	static char	*s;

	if (!(line))
		return (-1);
	ft_init(&truc, 1);
	if ((fd <= 2 && fd != 0) || read(fd, &test, 0) != 0)
		return (-1);
	*line = ft_strnew(1);
	return (ft_all(fd, &truc, line, &s));
}
