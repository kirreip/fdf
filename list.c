/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/16 11:41:39 by pmartin           #+#    #+#             */
/*   Updated: 2015/05/12 03:30:38 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_map	*newelem(int x, int y, int z, t_map *elem)
{
	if (!((*elem).nxt = (t_map *)malloc(sizeof(*elem))))
		return (0);
	(*elem).nxt->x = x;
	(*elem).nxt->y = y;
	(*elem).nxt->z = z;
	(*elem).nxt->prvs = elem;
	(*elem).nxt->nxt = 0;
	return ((*elem).nxt);
}

t_map	*first_elem(void)
{
	t_map *elem;

	if (!(elem = (t_map *)malloc(sizeof(*elem))))
		return (0);
	return (elem);
}
