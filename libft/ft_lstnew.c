/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/17 16:52:04 by pmartin           #+#    #+#             */
/*   Updated: 2014/11/19 19:19:03 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list *ft_lstnew(void const *content, size_t content_size)
{
	t_list *list_temp;

	if (!(list_temp = malloc(sizeof(*list_temp))))
		return (0);
	ft_memcpy(list_temp->content, content, sizeof(content));
	list_temp->content_size = content_size;
	return (list_temp);
}
