/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/08 00:36:07 by pmartin           #+#    #+#             */
/*   Updated: 2014/11/08 02:49:30 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int ft_memcmp(const void *s1, const void *s2, size_t n)
{
	const unsigned char *ts1;
	const unsigned char *ts2;

	ts1 = (unsigned char *)s1;
	ts2 = (unsigned char *)s2;
	while (n--)
		if (*ts1++ != *ts2++)
			return (*--ts1 - *--ts2);
	return (0);
}
