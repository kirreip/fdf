/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/09 01:40:57 by pmartin           #+#    #+#             */
/*   Updated: 2014/11/21 12:10:32 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		ft_isspc(char c)
{
	return ((c == ' ' || c == '\t' || c == '\n') ? 1 : 0);
}

char			*ft_strtrim(char const *s)
{
	int		i;
	int		j;
	char	*str;

	i = 0;
	j = ft_strlen(s) - 1;
	if (!(ft_isspc(s[i])) && !(ft_isspc(s[j])))
		return (ft_strdup(s));
	while (ft_isspc(s[i]) || ft_isspc(s[j]))
		(ft_isspc(s[i])) ? i++ : j--;
	if (i == (int)ft_strlen(s))
		return (str = ft_strnew(1));
	if (!(str = ft_strnew(j - i + 2)))
		return (0);
	return (ft_strncpy(str, &s[i], j - i + 1));
}
