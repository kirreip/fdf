/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 00:51:59 by pmartin           #+#    #+#             */
/*   Updated: 2014/11/09 08:30:40 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char		*transtr(char *s)
{
	int i;
	int temp;

	i = 0;
	while (s[i])
	{
		if ((double)s[i] < 0)
		{
			temp = (int)s[i];
			s[i] = temp * -1;
		}
		if ((double)s[i] >= 256)
		{
			temp = ((int)s[i] % ((int)s[i] / 256));
			s[i] = temp;
		}
		i++;
	}
	return ((char *)s);
}

static char		*ft_magie(char *dst, const char *src)
{
	int i;

	i = 0;
	while (src[i] != '\0')
	{
		dst[i] = src[i];
		i++;
	}
	dst[i] = '\0';
	return (dst);
}

int				ft_atoi(const char *st)
{
	int		i;
	int		nb;
	int		cond;
	char	*s;

	i = 0;
	nb = 0;
	cond = 0;
	if (!(s = (char *)malloc(sizeof(*st) * ft_strlen(st))) || st[0] == 0)
		return (0);
	s = ft_magie(s, st);
	s = transtr(s);
	while ((s[i] < 14 && s[i] > 8) || s[i] == 32)
		i++;
	while ((ft_isdigit((char)s[i])) || ((s[i] == 43 || s[i] == 45) && i == 0))
	{
		if ((s[0] == 43 || s[0] == 45) && i == 0)
			(s[i++] == 45) ? (cond = 1) : (nb);
		(ft_isdigit(s[i])) ? (nb = nb * 10 + s[i++] - 48) : (nb);
	}
	(cond == 1) ? (nb = nb * -1) : (nb);
	free(s);
	return (nb);
}
