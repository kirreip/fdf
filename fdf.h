/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/16 10:57:43 by pmartin           #+#    #+#             */
/*   Updated: 2015/05/12 21:09:32 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H
# include "libft/includes/libft.h"
# include <mlx.h>
# include <stdlib.h>
# include <fcntl.h>
# include <sys/types.h>

typedef struct		s_map
{
	struct s_map	*nxt;
	struct s_map	*prvs;
	int				x;
	int				y;
	int				z;
}					t_map;
typedef struct		s_var
{
	t_map			*elem;
	t_map			*temp;
	double			x1;
	double			x2;
	int				xtemp;
	double			y1;
	double			y2;
	int				ytemp;
	double			i;
	double			j;
	int				u;
	double			cst[2];
	int				endian;
	int				sizeline;
	int				bpp;
	void			*img_ptr;
	void			*mlx_ptr;
	void			*win_ptr;
	char			*data;
	double			a;
	double			b;
	int				xmax;
	int				project;
}					t_var;
t_map				*first_elem();
t_map				*newelem(int x, int y, int z, t_map *elem);
void				ft_printmap(t_map *elem, t_var var);
int					get_next_line(int fd, char **s);
int					expose_hook(t_var *var);
int					key_hook(int k, t_var *var);
void				ft_line(t_var var, t_map *elem);
void				ft_line3(t_var var, t_map *elem);
char				**ft_strfdf(char *s);
void				ft_pixel_put(t_var var);
void				ft_makeline(t_var var);
#endif
