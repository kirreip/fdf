/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/26 13:26:42 by pmartin           #+#    #+#             */
/*   Updated: 2015/05/12 23:47:29 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		checkpoint(t_var var)
{
	if (var.ytemp >= 0 && var.xtemp >= 0 && var.xtemp <= 1999
		&& var.ytemp <= 1199)
		return (1);
	return (0);
}

void	ft_makeline(t_var var)
{
	if (var.x2 - var.x1 != 0)
		var.a = (var.y2 - var.y1) / (var.x2 - var.x1);
	else
		var.a = 2099999999;
	var.b = var.y1 - (var.a * var.x1);
	var.xtemp = var.x1;
	var.ytemp = var.y1;
	if (var.a < 1 && var.a > -1)
		while (var.xtemp != (int)var.x2)
		{
			var.ytemp = var.a * var.xtemp + var.b;
			if (checkpoint(var))
				ft_pixel_put(var);
			(var.x1 < var.x2) ? var.xtemp++ : var.xtemp--;
		}
	else
		while (var.ytemp != (int)var.y2)
		{
			var.xtemp = (var.ytemp - var.b) / var.a;
		if (checkpoint(var))
			ft_pixel_put(var);
		(var.y1 < var.y2) ? var.ytemp++ : var.ytemp--;
		}
}

void	ft_line2(t_var var, t_map *elem, int x)
{
	if (x > 0)
		ft_line2(var, elem, x - 1);
	while (elem->x < x)
		elem = elem->nxt;
	var.x2 = (var.cst[0] * elem->x - var.cst[1] * elem->y) * var.u + var.i;
	var.y2 = (elem->z * -1 + elem->x * var.cst[0] / 2
			+ elem->y * var.cst[1] / 2) * var.u + var.j;
	while (elem)
	{
		var.x1 = var.x2;
		var.y1 = var.y2;
		var.temp = elem;
		elem = elem->nxt;
		while (elem && elem->x != x)
			elem = elem->nxt;
		if (elem)
		{
			var.x2 = (var.cst[0] * elem->x - var.cst[1] * elem->y)
				* var.u + var.i;
			var.y2 = (elem->z * -1 + elem->x * var.cst[0] / 2
				+ elem->y * var.cst[1] / 2) * var.u + var.j;
			if (var.temp->y + 1 == elem->y)
				ft_makeline(var);
		}
	}
}

void	ft_line(t_var var, t_map *elem)
{
	t_map *temp;

	var.xmax = 0;
	temp = elem;
	while (elem->nxt)
	{
		if (elem->nxt->x > var.xmax)
			var.xmax = elem->nxt->x;
		var.x1 = (var.cst[0] * elem->x - var.cst[1] * elem->y) * var.u + var.i;
		var.x2 = (var.cst[0] * elem->nxt->x
				- var.cst[1] * elem->nxt->y) * var.u + var.i;
		var.y1 = (elem->z * -1 + elem->x * var.cst[0] / 2
				+ elem->y * var.cst[1] / 2) * var.u + var.j;
		var.y2 = (elem->nxt->z * -1 + elem->nxt->x * var.cst[0] / 2
				+ elem->nxt->y * var.cst[1] / 2) * var.u + var.j;
		if (elem->y == elem->nxt->y && elem->x == (elem->nxt->x - 1))
			ft_makeline(var);
		elem = elem->nxt;
	}
	ft_line2(var, temp, var.xmax);
}

void	ft_printmap(t_map *elem, t_var var)
{
	elem = elem->nxt;
	var.elem = elem;
	var.cst[1] = 0.9;
	var.cst[0] = 0.9;
	var.mlx_ptr = mlx_init();
	var.img_ptr = mlx_new_image(var.mlx_ptr, 2000, 1200);
	var.data = mlx_get_data_addr(var.img_ptr, &var.bpp,
				&var.sizeline, &var.endian);
	var.win_ptr = mlx_new_window(var.mlx_ptr, 2000, 1200, "FDF");
	if (elem->nxt)
		ft_line(var, elem);
	else
		mlx_pixel_put(var.mlx_ptr, var.win_ptr, var.i, var.j, 0xFFFFFF);
	mlx_put_image_to_window(var.mlx_ptr, var.win_ptr, var.img_ptr, 0, 0);
	mlx_expose_hook(var.win_ptr, expose_hook, &var);
	mlx_key_hook(var.win_ptr, key_hook, &var);
	mlx_loop(var.mlx_ptr);
}
